<!DOCTYPE html>
<html>
<head>
	<title>Dim Sum Pinyin Search</title>
</head>
<link rel="stylesheet" href="./style.css?a=c">
<body>

    <div class="container">
      <input type="text" name="words" placeholder="Search..." id="searchbox">
      <div class="search"></div>
      <div class="livesearch"></div>
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script type="text/javascript">
	var searchRequest = null;

    $(function () {
        var minlength = 2;

        $("#searchbox").keypress(function (e) {
            var KeyID = e.keyCode;
            if(KeyID==13 || KeyID==32){
                var that = this,
                value = $(this).val();

                if (value.length >= minlength ) {
                    if (searchRequest != null) 
                        searchRequest.abort();
                    searchRequest = $.ajax({
                        type: "post",
                        url: "pinyin.php",
                        data: {
                            'words' : value
                        },
                        dataType: "text",
                        success: function(msg){
                            $('.livesearch').html(msg);
                        }
                    });
                }
            }
        });
    });
</script>
</html>